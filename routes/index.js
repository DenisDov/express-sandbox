const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	const readableBytes = require("../utils/readableBytes");
	const pageData = {
		title: "Homepage",
		message: "Homepage here",
		userLocale: req.acceptsLanguages()[1],
		memoryUsed: readableBytes(process.memoryUsage().heapUsed)
	};

	res.render("index", pageData);
});

router.get("/about", (req, res) => {
	res.render("about", { title: "About", message: "About page" });
});

router.get("/info", (req, res) => {
	const myURL = new URL("http://example.com/hello.html?id=8f35490242&status=active");
	const refID = myURL.searchParams.get("id");
	const data = {
		refID
	};
	res.render("info", { title: "Info", data });
});

var cb = function(req, res, next) {
	// COWSAY
	// var cowsay = require("cowsay");
	// console.log(
	//     cowsay.say({
	//         text: "sandbox route callback",
	//         e: "oO",
	//         T: "U "
	//     })
	// );
	// PROGRESS
	// const ProgressBar = require("progress");
	// const bar = new ProgressBar(":bar", { total: 10 });
	// const timer = setInterval(() => {
	//     bar.tick();
	//     if (bar.complete) {
	//         clearInterval(timer);
	//     }
	// }, 100);

	// ORA
	// const ora = require("ora");
	// const spinner = ora();

	// FETCH
	// const fetch = require("node-fetch");

	// fetch("https://jsonplaceholder.typicode.com/todos/1")
	//     .then(spinner.start("fetching todos"))
	//     .then(response => response.json())
	//     .then(json => {
	//         console.log(json);
	//         spinner.succeed("done todos");
	//     })
	//     .catch(err => {
	//         spinner.fail(err.code);
	//     });

	// AXIOS
	// const axios = require("axios");
	// axios
	//     .get("https://jsonplaceholder.typicode.com/todos/1")
	//     .then(spinner.start("fetching todos"))
	//     .then(json => {
	//         // console.log(json.data);
	//         spinner.succeed("done todos");
	//     })
	//     .catch(err => {
	//         spinner.fail(err.code);
	//     });

	next();
};

router.get("/sandbox", cb, (req, res) => {
	const ora = require("ora");
	const spinner = ora();
	const axios = require("axios");
	let users;
	axios
		.get("https://jsonplaceholder.typicode.com/users")
		// .then(spinner.start("fetching todos"))
		.then(json => {
			// console.log(json.data);
			users = json.data;
			// spinner.succeed("done todos");
			res.render("sandbox", {users})
		})
		.catch(err => {
			console.log('err: ', err);
			// spinner.fail(err.code);
		});
	// console.log("cb: ", cb);
	// console.log('users: ', typeof users);
	// res.render("sandbox")
});

module.exports = router;
