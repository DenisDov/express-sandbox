require("dotenv").config({ path: "./config/.env" });
// OR
// const PORT = require("./config/options.json5").port;
const PORT = process.env.PORT || 5000;
if (process.env.NODE_ENV === "production") console.log("Server started in production mode");
else {
    require("console-stamp")(console, "mm-dd HH:MM:ss");
    console.log(`Server started in dev mode on port ${PORT}`);
}
const createError = require("http-errors");
const express = require("express"),
    app = express();
const path = require("path");
const mysql = require("mysql");

// const logger = require("./utils/logger");

// app.use(logger); // Middleware

// Body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Enable pug templates
app.set("view engine", "pug");

// Set static folder for css,js,imgs & compile less
app.use(require("less-middleware")(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public")));

// Routes
app.use("/", require("./routes/index"));
app.use("/api/users", require("./routes/api/users"));

// const PORT = require("./config/options.json5").port;
// // const PORT = process.env.PORT || 5000;

// Connect to MySQL
const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "kT430001",
    database: "demo"
});

db.connect(err => {
    if (err) {
        console.error(err.stack);
        throw err;
    }
    console.log("connected as id " + db.threadId);
});

app.get("/users", (req, res) => {
    const sql = "SELECT * FROM demo.competitors LIMIT 10;";
    
    db.query(sql, (err, result) => {
        console.log('result: ', result.length);
        if (err) throw err;
        res.render("users", { title: "Users", result });
    });
    // db.end(function(err) {
    //     if (err) throw err;
    //     console.log("db closed");
    // });
});
// END MySQL code

// Handle 404 page
// app.use((req, res, next) => {
//     res.status(404);
//     res.render("404", { title: "404" });
// });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    if (err.status == "404") {
        res.status(404);
        res.render("404", { title: "404" });
    } else {
        res.status(500);
        res.render("error", { title: "500" });
    }
});

app.listen(PORT);
