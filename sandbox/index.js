const fs = require("fs");
const path = require("path");

// Create folder: run `node sandbox/index`
// fs.mkdir(path.join(__dirname, 'test'), err => {
//   if(err) throw err;
//   console.log('Folder created...');
// })

let filePath = path.join(__dirname, "test", "text.txt");

// fs.writeFile(filePath, "Hello Node", err => {
//     if (err) throw err;
//     // console.log("Text created...");
// });

// fs.appendFile(filePath, "\nAppend Node", err => {
//     if (err) throw err;
//     // console.log("Append text...");
// });

fs.writeFile(filePath, '\nAdded new content to end', { flag: 'a+' }, (err) => {
    if (err) throw err;
})

fs.readFile(filePath, "utf-8", (err, content) => {
    if (err) throw err;
    console.log(content);
});

fs.stat(filePath, (err, stats) => {
    if (err) throw err;
    console.log("stats: ", stats);
});

fs.readdir(__dirname, (err, files) => {
    if (err) throw err;
    console.log('files: ', files);
})